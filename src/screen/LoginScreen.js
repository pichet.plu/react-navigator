import React, { Component } from "react";
import { StyleSheet, Text, View ,Button} from "react-native";


class LoginScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
       <Button title="go to home" onPress={()=> this.props.navigation.navigate('Login')}></Button>
      </View>
    );
  }
}
export default LoginScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  }
});
